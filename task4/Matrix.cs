using System;
using System.Text;

namespace ShlemkevychSerhiy.PracticeTasks.Task4
{
    class Matrix
    {
        private double[,] _array;

        public Matrix(int rowCount, int columnCount)
        {
            _array = new double[rowCount, columnCount];
        }

        public int GetLength(int dimension)
        {
            return _array.GetLength(dimension);
        }

        public double this[int row, int column]
        {
            get
            {
                return _array[row, column];
            }
            set
            {
                _array[row, column] = value;
            }
        }

        public void Transpose()
        {
            double[,] result = new double[GetLength(1), GetLength(0)];

            for (var i = 0; i < result.GetLength(0); i++)
            {
                for (var j = 0; j < result.GetLength(1); j++)
                {
                    result[i, j] = this[j, i];
                }
            }

            _array = result;
        }

        public void Multiply(Matrix matrix)
        {
            if (GetLength(1) != matrix.GetLength(0))
            {
                throw new InvalidOperationException();
            }

            double[,] result = new double[GetLength(0), matrix.GetLength(1)];

            for (var i = 0; i < GetLength(0); i++)
            {
                for (var j = 0; j < matrix.GetLength(1); j++)
                {
                    for (var k = 0; k < GetLength(1); k++)
                    {
                        result[i, j] += this[i, k] * matrix[j, k];
                    }
                }
            }

            _array = result;
        }

        public void Substract(Matrix matrix)
        {
            if (GetLength(0) != matrix.GetLength(0) || GetLength(1) != matrix.GetLength(1))
            {
                throw new InvalidOperationException();
            }

            for (var i = 0; i < GetLength(0); i++)
            {
                for (var j = 0; j < GetLength(1); j++)
                {
                    this[i, j] -= matrix[i, j];
                }
            }
        }

        public void Add(Matrix matrix)
        {
            if (GetLength(0) != matrix.GetLength(0) || GetLength(1) != matrix.GetLength(1))
            {
                throw new InvalidOperationException();
            }

            for (var i = 0; i < GetLength(0); i++)
            {
                for (var j = 0; j < GetLength(1); j++)
                {
                    this[i, j] += matrix[i, j];
                }
            }
        }

        public void ScalarMultiply(double multipler)
        {
            for (var i = 0; i < GetLength(0); i++)
            {
                for (var j = 0; j < GetLength(1); j++)
                {
                    this[i, j] *= multipler;
                }
            }
        }

        public void CreateSubmatrix(int[] rows, int[] columns)
        {
            double[,] result = new double[rows.Length, columns.Length];

            for (var i = 0; i < rows.Length; i++)
            {
                for (var j = 0; j < columns.Length; j++)
                {
                    result[i, j] = this[rows[i], columns[j]];
                }
            }

            _array = result;
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("Matrix:");

            for (var i = 0; i < GetLength(0); i++)
            {
                builder.Append(String.Format("\nRow #{0}:", i));
                for (var j = 0; j < GetLength(1); j++)
                {
                    builder.Append(String.Format(" {0},", this[i, j]));
                }
            }

            return builder.ToString();
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            Matrix matrix = obj as Matrix;

            if (matrix == null)
            {
                return false;
            }

            if (matrix.GetLength(0) != GetLength(0) || matrix.GetLength(1) != GetLength(1))
            {
                return false;
            }

            for (var i = 0; i < GetLength(0); i++)
            {
                for (var j = 0; j < GetLength(1); j++)
                {
                    if (this[i, j] != matrix[i, j])
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public static Matrix Transpose(Matrix matrix)
        {
            Matrix result = new Matrix(matrix.GetLength(1), matrix.GetLength(0));

            for (var i = 0; i < result.GetLength(0); i++)
            {
                for (var j = 0; j < result.GetLength(1); j++)
                {
                    result[i, j] = matrix[j, i];
                }
            }

            return result;
        }

        public static Matrix Multiply(Matrix left, Matrix right)
        {
            if (left.GetLength(1) != right.GetLength(0))
            {
                throw new InvalidOperationException();
            }

            Matrix result = new Matrix(left.GetLength(0), right.GetLength(1));

            for (var i = 0; i < left.GetLength(0); i++)
            {
                for (var j = 0; j < right.GetLength(1); j++)
                {
                    for (var k = 0; k < left.GetLength(1); k++)
                    {
                        result[i, j] += left[i, k] * right[j, k];
                    }
                }
            }

            return result;
        }

        public static Matrix Substract(Matrix left, Matrix right)
        {
            if (left.GetLength(0) != right.GetLength(0) || left.GetLength(1) != right.GetLength(1))
            {
                throw new InvalidOperationException();
            }

            Matrix result = new Matrix(left.GetLength(0), left.GetLength(1));

            for (var i = 0; i < left.GetLength(0); i++)
            {
                for (var j = 0; j < left.GetLength(1); j++)
                {
                    result[i, j] = left[i, j] - right[i, j];
                }
            }

            return result;
        }

        public static Matrix Add(Matrix left, Matrix right)
        {
            if (left.GetLength(0) != right.GetLength(0) || left.GetLength(1) != right.GetLength(1))
            {
                throw new InvalidOperationException();
            }

            Matrix result = new Matrix(left.GetLength(0), left.GetLength(1));

            for (var i = 0; i < left.GetLength(0); i++)
            {
                for (var j = 0; j < left.GetLength(1); j++)
                {
                    result[i, j] = left[i, j] + right[i, j];
                }
            }

            return result;
        }

        public static Matrix ScalarMultiply(Matrix matrix, double multipler)
        {
            Matrix result = new Matrix(matrix.GetLength(0), matrix.GetLength(1));

            for (var i = 0; i < matrix.GetLength(0); i++)
            {
                for (var j = 0; j < matrix.GetLength(1); j++)
                {
                    result[i, j] = matrix[i, j] * multipler;
                }
            }

            return result;
        }

        public static Matrix CreateSubmatrix(Matrix matrix, int[] rows, int[] columns)
        {
            Matrix result = new Matrix(rows.Length, columns.Length);

            for (var i = 0; i < rows.Length; i++)
            {
                for (var j = 0; j < columns.Length; j++)
                {
                    result[i, j] = matrix[rows[i], columns[j]];
                }
            }

            return result;
        }

        public static Matrix operator +(Matrix left, Matrix right)
        {
            return Add(left, right);
        }

        public static Matrix operator -(Matrix left, Matrix right)
        {
            return Substract(left, right);
        }

        public static Matrix operator *(Matrix left, Matrix right)
        {
            return Multiply(left, right);
        }

        public static Matrix operator *(Matrix matrix, double multipler)
        {
            return ScalarMultiply(matrix, multipler);
        }

        public static Matrix operator *(double multipler, Matrix matrix)
        {
            return ScalarMultiply(matrix, multipler);
        }

        public static bool operator ==(Matrix left, Matrix right)
        {
            if (System.Object.ReferenceEquals(left, right))
            {
                return true;
            }

            if ((object)left == null || (object)right == null)
            {
                return false;
            }

            return left.Equals(right);
        }

        public static bool operator !=(Matrix left, Matrix right)
        {
            if (System.Object.ReferenceEquals(left, right))
            {
                return false;
            }

            if ((object)left == null || (object)right == null)
            {
                return true;
            }

            return !left.Equals(right);
        }
    }

    class Program
    {
        static void Main()
        {
            Matrix a = new Matrix(2, 2);
            Matrix b = new Matrix(2, 2);

            a[0, 0] = 1;
            a[0, 1] = 5;
            a[1, 0] = 2;
            a[1, 1] = 9;

            b[0, 0] = 5;
            b[0, 1] = 6;
            b[1, 0] = 3;
            b[1, 1] = 8;

            Console.WriteLine("Let`s create matrix a: {0}\nand b: {1}", a.ToString(), b.ToString());

            Console.WriteLine("2 * a * b + b - a = {0}", (2 * a * b + b - a).ToString());

            Console.WriteLine("Are a and b equal? - {0}", a.Equals(b) ? "yes" : "no");

            Console.WriteLine("Let`s cross out second column from matrix b and transpose the result: {0}", (Matrix.Transpose(Matrix.CreateSubmatrix(b, new int[] { 0, 1 }, new int[] { 0 }))).ToString());

            Console.ReadKey();
        }
    }
}